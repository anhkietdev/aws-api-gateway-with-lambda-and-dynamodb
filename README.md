# Cloud Computing là gì ?

- Khi việc phát triển website ngày một lớn mạnh, việc cần quan tâm chính là khả năng mở rộng server để phục vụ cho nhu cầu đó. Nhà phát triển sẽ phải bỏ ra một số chi phí rất lớn như hệ thống làm mát, bảo trì, thêm và thay đổi phần cứng, chi phí thuê và phải đối mặt với một số thiên tai. <br>
  ==> **Cloud Computing(AWS, Azure, Google Cloud)** ra đời để giải quyết bài toán đó.
- `Cloud computing is the on-demand(get it when need it) delivery of compute power, database storage, applications, and other IT resources`
- Cloud computing phục vụ theo nguyên tắc **Pay-as-you-go pricing**, chỉ trả tiền cho những gì chúng ta yêu cầu, khi sử dụng xong thì không cần trả nữa.

# IAM (Identity and Access Management)

- IAM là một Global service cung cấp các công cụ để quản lý và kiểm soát quyền truy cập vào tài nguyên AWS.
- Một số điểm chính về IAM:
  - **Users**: cho phép tạo và quản lý các tài khoản người dùng để nhận diện và xác minh ai có quyền truy cập tài nguyên AWS.
  - **Groups**: nhóm các người dùng lại với nhau, quản lý tài nguyên bằng cách áp dụng chung cho một nhóm.
  - **Roles**: cho phép xác định một tập hợp các quyền truy cập và sau đó gán vai trò này cho các nguồn tài nguyên cụ thể, như EC2 hoặc Lambda.
  - **Policies**: mô tả những hành động nào có thể được thực hiện và trên những tài nguyên nào. Có thể gán cho users, groups, roles.

# EC 2 (Elastic Compute Cloud)

- EC2 là một dịch vụ cung cấp các phiên bản máy ảo và có thể cấu hình dung lượng bộ nhớ và các thông số liên quan(OS, CPU, RAM, Sercurity...). Tùy vào mục đích sử dụng mà người dùng có thể yêu cầu các thông số cấu hình sao cho phù hợp.
- EC2 bao gồm các chức năng:
  - Thuê máy ảo - **EC2**.
  - Lưu trữ dữ liệu trên máy ảo - **EBS**.
  - Phân phối khả năng tải trên Cloud - **ELB**.
  - Mở rộng quy mô dịch vụ bằng cách sử dụng auto-scaling group - **ASG**.

# S3 (Simple Storage Service)

- S3 là một dịch vụ lưu trữ dạng **Object Storage** cung cấp một nơi để lưu trữ và truy xuất dữ liệu.
- Mỗi đối tượng lưu trữ trên S3 bao gồm Blob và MetaData của dữ liệu đó.
- Các dữ liệu trong S3 được truy cập thông qua phương thức HTTP và HTTPS. <br>
  ==> **Truy cập từ mọi nơi**.

# AWS Lambda – Serverless Compute

- Kiến trúc Serverless ra đời phục vụ cho bài toán: các doanh nghiệp hay lập trình viên chỉ cần quan tâm đến việc xây dựng ứng dụng, sản phẩm thay vì lo lắng đến việc quản lý và vận hành máy chủ.
- Why Lambda ?
  - Virtual functions – no servers to manage!
  - Limited by time - short executions.
  - Run on-demand.
  - Scaling is automated!
- Code chạy trên Lambda gọi là Lambda function.
- Lambda function được trigger bởi các **Events**. Các events có thể xảy ra từ nhiều nguồn khác nhau, có thể từ **S3 Bucket**, thay đổi trong **DynamoDB**, call từ **API Gateway**.
- Sau khi Lambda function được trigger, AWS tạo ra một execution environment cho function đó, bao gồm tất cả những gì cần thiết để chạy được code, ngôn ngữ, thư viện, dependencies,...
- Sau khi code được thực thi xong, execution environment sẽ bị hủy bỏ, AWS Lambda sẽ quyết định số lượng environment cần thiết để xử lý sự kiện, có thể mở rộng khi cần.

# DynamoDB

- DynamoDB là một NoSQL service được cung cấp bởi AWS, dùng để tạo ra các bảng có khả năng lưu trữ và truy xuất dữ liệu.
- Nói cách khác, DynamoDB đáp ứng mọi nhu cầu của một hệ quản trị cơ sở dữ liệu thông dụng. Nó được authorized bằng IAM và có thể tương tác rất tốt với các dịch vụ khác của AWS.

# API Gateway

- APIs Gateway làm nhiệm vụ nhận các requests từ phía client, chỉnh sửa, xác thực và điều hướng chúng đến các API cụ thể trên các services phía sau. Vì thế API có một số lợi ích:
  - Che giấu được cấu trúc của hệ thống với bên ngoài, clients sẽ tương tác với hệ thống thông qua api gateway chứ không gọi trực tiếp tới một services cụ thể.
  - Request caching và cân bằng tải.
  - Có thể sử dụng các cơ chế xác thực người dùng của API Gateway thay vì dùng service.

# API Gateway Demo

- Tạo project AWS Lambda Project

![0](img/0.png)

![1](img/1.png)

- Thêm các package:

![2](img/2.png)

- Tạo mới một class Entity, các annotation phải trùng với các attributes trên DynamoDB

![3](img/3.png)

- Refactor class Function, có thể cập nhật thêm Delete,...

![4](img/4.png)

- Publish to AWS Lambda

![5](img/5.png)

- Đặt tên Function

![6](img/6.png)

- Chọn AWSLambdaBasicExecutionRole

![7](img/7.png)

- Open AWS Management Console ==> Lambda ==> Sẽ thấy function vừa upload

![8](img/8.png)

- Chọn vào đây

![9](img/9.png)

- Thêm Permisson

![10](img/10.png)

- Tìm và thêm vào AmazonDynamoDBFullAccess

![11](img/11.png)

- Tạo một table trong DynamoDB

![12](img/12.png)

- Data hiện tại:

![13](img/13.png)

- Tạo mới một API Gateway tên customer-manager

![14](img/14.png)

- Tạo mới 2 route tương tự

![15](img/15.png)

- Chọn Attach Integration => Chọn Lambda => chọn function tên customer-manager

- Quay lại API, nhấn vào Invoke URL

![16](img/16.png)

- Thêm “/customers” vào cuối URL và nhận kết quả

![17](img/17.png)

- Để gọi hàm Create, chúng ta sử dụng Postman.

- Thêm request và chỉnh sửa json body cho phù hợp

![18](img/18.png)

![19](img/19.png)

- Sau đó có thể quay lại default URL để check sự thay đổi.
